FROM openjdk:8-jre-alpine

RUN mkdir /mytest
WORKDIR /mytest
COPY runner/target/runner.jar ./runner.jar

ENTRYPOINT [ "java", "-jar", "runner.jar"]