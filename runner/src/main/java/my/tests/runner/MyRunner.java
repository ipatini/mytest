package my.tests.runner;

import my.tests.dep.MyDependency;

public class MyRunner {
    public static void main(String[] args) {
        (new MyDependency()).testDependency();
    }
}
